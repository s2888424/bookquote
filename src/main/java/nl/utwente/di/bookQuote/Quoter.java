package nl.utwente.di.bookQuote;

import java.util.HashMap;

public class Quoter {

    HashMap<String,Double> store = new HashMap<>();

    public Quoter(){
        store.put("1", 10.0);
        store.put("2", 45.0);
        store.put("3", 20.0);
        store.put("4", 35.0);
        store.put("5", 50.0);
//        if()
//        for(int i = 1; i<;i++){
//            String f = String.valueOf(i);
//            switch (f){
//                case "1":
//                    store.put(f, 10.0);
//                    break;
//                case "2":
//                    store.put(f, 45.0);
//                    break;
//                case "3":
//                    store.put(f, 20.0);
//                    break;
//                case "4":
//                    store.put(f, 35.0);
//                    break;
//                case "5":
//                    store.put(f, 50.0);
//                    break;
//                case "6":
//                    store.put(f, 0.0);
//                    break;
//            }
//        }
    }
    public Double getBookPrice(String isbn){
        if(store.containsKey(isbn)){
            return store.get(isbn);
        }
        else{
            return 0.0;
        }
    }
}

